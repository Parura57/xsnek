#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>


struct Cell {
    int x;
    int y;
    int active;
};
short cont = 1;
int size = 20;
int sneksize = 0;
int direction = 0; // 0 = right, rotates counterclockwise

void NewApple (struct Cell *appleptr, struct Cell snek[]) {
    srand(time(NULL));
    appleptr->x = rand() % 600/size;
    appleptr->y = rand() % 400/size;

    int i = 0;
    while(snek[i].active) {    
        if (appleptr->x == snek[i].x &&
            appleptr->y == snek[i].y) NewApple(appleptr, snek);
        i++;
    }
    //printf("appl %i %i random %i %i\n", appleptr->x, appleptr->y, random(), random());
}

void KeyHandler(XEvent e) {
    //printf("%i\n", direction);
    switch (e.xkey.keycode) {
        case 111: if(direction != 3) direction = 1; break; // Up
        case 113: if(direction != 0) direction = 2; break; // Left
        case 114: if(direction != 2) direction = 0; break; // Right
        case 116: if(direction != 1) direction = 3; break; // Down
        default: printf("invalid event %i\n", e.xkey.keycode);
    }
}

void render(Display *dpy, Window win, GC gc, int s, struct Cell snek[], struct Cell *appleptr) {
    XFlush(dpy);
    // Clear screen
    XSetForeground(dpy, gc, BlackPixel(dpy, s));
    XFillRectangle(dpy, win, gc, 0, 0, 600, 400);

    // Render stuff
    /*XSetForeground(dpy, gc, 0x00FFCC);
    XDrawRectangle(dpy, win, gc, 10, 15, 20, 25);
    XFillRectangle(dpy, win, gc, 300, 0, 200, 300);
    XDrawPoint(dpy, win, gc, 5, 10);*/

    // Update snek
    for (int i = sneksize; i > 0; i--) {
        snek[i].x = snek[i-1].x;
        snek[i].y = snek[i-1].y;
    }
    switch (direction) {
        case 0: // Right
            snek[0].x++;
            if (snek[0].x == snek[1].x && snek[0].y == snek[1].y ||
                snek[0].x == snek[2].x && snek[0].y == snek[2].y
               ) {snek[0].x -=2; direction = 2;}
            break;

        case 1: // Up
            snek[0].y--;
            if (snek[0].x == snek[1].x && snek[0].y == snek[1].y ||
                snek[0].x == snek[2].x && snek[0].y == snek[2].y
               ) {snek[0].y +=2; direction = 3;}
            break;

        case 2: // Left
            snek[0].x--;
            if (snek[0].x == snek[1].x && snek[0].y == snek[1].y ||
                snek[0].x == snek[2].x && snek[0].y == snek[2].y
               ) {snek[0].x +=2; direction = 0;}
            break;

        case 3: // Down
            snek[0].y++;
            if (snek[0].x == snek[1].x && snek[0].y == snek[1].y ||
                snek[0].x == snek[2].x && snek[0].y == snek[2].y
               ) {snek[0].y +=2; direction = 1;}
            break;

        default:
            printf("invalid direction\n");
    }

    // Snek collision
    if (snek[0].x < 0 ||
        snek[0].y < 0 ||
        snek[0].x > 600/size ||
        snek[0].y > 400/size
       ) cont = 0;
    int i = 0;
    int j = 0;
    while(snek[i].active) {
        while(snek[j].active) {
            if (snek[i].x == snek[j].x &&
                snek[i].y == snek[j].y &&
                i != j
               ) {/*printf("Twoja stara gryzie ogon\n")*/; cont = 0;}
            j++;
        }
        i++;
    }
    i = 0;
    while(snek[i].active) {/*printf("%i: x%i y%i ", i, snek[0].x, snek[0].y);*/ i++;}
    //printf("\ndirection %i\n", direction);

    // Apple collision
    if (appleptr->x == snek[0].x &&
        appleptr->y == snek[0].y) {
        NewApple(appleptr, snek);
        // Make snek longer
        snek[sneksize].active = 1;
        sneksize++;
     }

    // Render snek
    XSetForeground(dpy, gc, 0x00FFCC);
    i = 0;
    while(snek[i].active) {
        XFillRectangle(dpy, win, gc, snek[i].x*size, snek[i].y*size, size, size);
        i++;
    }

    // Render apple
    XSetForeground(dpy, gc, 0xFF0000);
    XFillRectangle(dpy, win, gc, appleptr->x*size, appleptr->y*size, size, size);
    //printf("%i %i\n", appleptr->x, appleptr->y);

    // Render score
    XSetForeground(dpy, gc, 0xFFFFFF);
    char scorestr[64];
    sprintf(scorestr, "%d", sneksize - 5);
    //XDrawString(dpy, win, gc, 0, 10, strcat("Score: ", scorestr), 7 + strlen(scorestr));
    XDrawString(dpy, win, gc, 0, 10, "Score: ", 7);
    XDrawString(dpy, win, gc, 50, 10, scorestr, strlen(scorestr));

    XFlush(dpy);
}

int main () {
    // Initialize window
    Display *dpy = XOpenDisplay(getenv("DISPLAY"));
    int s = DefaultScreen(dpy);
    Window win = XCreateSimpleWindow(dpy, RootWindow(dpy, s), 10, 10, 600, 400, 1, BlackPixel(dpy, s), WhitePixel(dpy,s));
    XStoreName(dpy, win, "XSnek");
    XSelectInput(dpy, win, ButtonPressMask|KeyPressMask|StructureNotifyMask|KeyReleaseMask|KeymapStateMask);
    XMapWindow(dpy, win);
    XFlush(dpy);

    // Initialize context
    XGCValues values;
    GC gc = XCreateGC(dpy, win, 0, &values);
    XSetForeground(dpy, gc, BlackPixel(dpy, s));
    //XSetBackground(dpy, gc, BlackPixel(dpy, s));
    XSetLineAttributes(dpy, gc, 2, LineSolid, CapButt, JoinBevel);
    XSetFillStyle(dpy, gc, FillSolid);

    // Variables declaration
    struct Cell snek[100];
    snek[0].x = 5; snek[0].y = 0; snek[0].active = 1;   // Starts at 4, because it's immediately updated, otherwise it would skip a cell
    snek[1].x = 4; snek[1].y = 0; snek[1].active = 1;
    snek[2].x = 3; snek[2].y = 0; snek[2].active = 1;
    snek[3].x = 2; snek[3].y = 0; snek[3].active = 1;
    snek[4].x = 1; snek[4].y = 0; snek[4].active = 1;
    sneksize = 5;
    struct Cell apple;

    // Main loop
    NewApple(&apple, snek);
    XEvent event;
    while(cont) {
        while (XPending(dpy) > 0) {
            XNextEvent(dpy, &event);
            if (event.type == ClientMessage) cont = 0;
            if (event.type == KeyPress) {
                //printf("keycode %i\n", event.xkey.keycode);
                KeyHandler(event);
            }
            if (event.type == KeymapNotify) XRefreshKeyboardMapping(&event.xmapping);
            //printf("%i\n", event.type);
        }
        XResizeWindow(dpy, win, 600, 400);
        render(dpy, win, gc, s, snek, &apple);
        usleep(0.18*1000000);     // Microseconds
        //usleep(1*1000000);     // Microseconds
   }

    // Exit
    XFlush(dpy);
    XDestroyWindow(dpy, win);
    XCloseDisplay(dpy);
    return 0;
}

